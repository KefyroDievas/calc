#include <iostream>
#include <iomanip>
#include <cmath>
#define pi 3.14159265
using namespace std;
float scan(string arg, int &i) {
    bool negative = false;
    float a = 0;

    if (arg[i] == '-') {
        negative = true;
        i++;
}
    while (arg[i] >= '0' && arg[i] <= '9') {
        a *= 10;
        a += int(arg[i]) - '0';
        i++;

        if (arg[i] == '.') {
            float fract = 1;
            i++;
            while (arg[i] >= '0' && arg[i] <= '9') {
                fract /= 10;
                fract *= int(arg[i]) - '0';
                a += fract;
                i++;
            }
        }
    }
    i--;
    if (negative) {
        a *=(-1);
}
    return a;
}
int main(){
    unsigned long long factorial = 1;
    string s;
    float f = 0;
    float a = 0;
    float ans = 0;
    cout << "To quit, type q\nTo see help, type h\n";
    while (1){
        factorial = 1;
        cout << "> ";
        getline(cin, s);
        for (int i = 0; s[i] != 0 ; i++){
            if (s[i] >= '0' && s[i] <= '9') {
                f = scan(s, i);
            }
            else {
                switch (s[i]){
                    case '+':
                        i++;
                        a = scan(s, i);
                        ans += a + f;
                        a = 0;
                        f = 0;
                        break;
                    case '-':
                        i++;
                        a = scan(s, i);
                        ans += f - a;
                        a = 0;
                        f = 0;
                        break;
                    case '*':
                        i++;
                        a = scan(s, i);
                        if (ans == 0)ans = 1;
                        ans *= a * f;
                        a = 1;
                        f = 1;
                        break;
                    case '/':
                        i++;
                        a = scan(s, i);
                        if (ans == 0)ans = 1;
                        ans *= f / a;
                        a = 1;
                        f = 1;
                        break;
                    case '^':
                        i++;
                        ans = 1;
                        a = scan(s, i);
                        ans = ans * pow(f, a);
                        a = 1;
                        f = 1;
                        break;
                    case 'V':
                        i++;
                        ans = 1;
                        a = scan(s, i);
                        ans = ans * pow(f, 1/a);
                        a = 1;
                        f = 1;
                        break;
                    case 'q':
                        cout << "quitting...\n";
                        return 0;
                        break;
                    case 's':
                        i++;
                        ans = sin(f * pi / 180);
                        a = 1;
                        f = 1;
                        break;
                    case 'c':
                        i++;
                        ans = cos(f * pi / 180);
                        a = 1;
                        f = 1;
                        break;
                    case 't':
                        i++;
                        ans = tan(f * pi / 180);
                        a = 1;
                        f = 1;
                        break;
                    case 'S':
                        ans = asin(f) * 180 / pi;
                        a = 1;
                        f = 1;
                        break;
                    case 'C':
                        ans = acos(f) * 180 / pi;
                        a = 1;
                        f = 1;
                        break;
                    case 'T':
                        ans = atan(f) * 180 / pi;
                        a = 1;
                        f = 1;
                        break;
                    case '!':
                        for(int i = 1; i <=f; ++i) {
                            factorial *= i;
                        }
                        ans = factorial;
                        break;
                    case 'l':
                        i++;
                        a = scan(s, i);
                        ans += log(a) / log(f) ;
                        a = 1;
                        f = 1;
                        break;
                    case 'h':
                        cout << " + for addition\n - for subtraction\n / for division by something\n * for multiplication\n ^ for power of something\n V for nth root of something\n s for sine\n c for cosine\n t for tangent\n S for sine-1\n C for cosine-1\n T for tangent-1\n l for logarithms\n ! for factorials\n This calculator is case sensitve, so you have to type in the exact characters\n";
                        break;
                    case 'e':
                        i++;
                        a = scan(s, i);
                        ans = f * pow(10.0, a);
                        a = 1;
                        f = 1;
                        break;
                    }
            }
        }
        cout << s << "=" << ans << endl;
        ans = 0;
        
    }
}
